<?php 
$pagetitle = "Malaysia Books of Records - About Us"; 
require_once('includes/header.php');
require_once('includes/navigation-main.php')
?>

<body>

	<!-- Human Achievement-banner -->
	<div class="page--human-achievement page--record-category">
		<div class="header-banner page--category-header-banner">
			<div class="header-banner-wrap">
				<div class="row">
					<div class="large-12">
						<div class="header-banner-image" style="background-image: url('images/human-achievements/EOCM.jpg')"></div>
							<div class="large-5 small-8 header-banner-title--container">
								<div class="header-banner-title--wrap">
									<div class="header-banner-title">
										<h5 class="page--subtitle">Record</h5>
										<h3 class="page--title">Human Achievements</h3>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="category-breadcrumb">
				<div class="row">
					<nav aria-label="You are here:" role="navigation">
						<ul class="breadcrumbs large-12 columns">
							<li>
								<a href="record.php">Record</a>
							</li>
							<li class="active">
								<a href="#">Human Achievements</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>


			<div class="row collapse">
				<div class="large-9 columns category-container">
					<div class="large-12">
						<div class="grid category-outer">
							<div class="grid-sizer"></div>

							<div class="large-2 grid-item brick category-item">
								<div class="category-record--item">
									<a href="record-detail.php">
										<div class="category-record-image">
											<img src="images/human-achievements/most-expensive-mooncake.png">
										</div>

										<div class="category-record-title">
											<span>Most Expensive Commercial Mooncake</span>
										</div>
										<div class="category-record-more-btn">
											<a href="record-detail.php">More +</a>
										</div>
									</a>
								</div>
							</div>

							<div class="large-2 grid-item brick category-item">
								<div class="category-record--item">
									<a href="#">
										<div class="category-record-image">
											<img src="images/human-achievements/DMR.jpg">
										</div>

										<div class="category-record-title">
											<span>Longest Distance Motorcycle Ride by A Group</span>
										</div>
										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>
								</div>
							</div>

							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="#">
										<div class="category-item-inner">
											<div class="category-record-image">
												<img src="images/human-achievements/HDC.jpg">
											</div>
										</div>

										<div class="category-record-title">
											<span>Biggest Handmade Dragon Collage</span>
										</div>
										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>


							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/6PB.jpg">
										</div>

										<div class="category-record-title">
											<span>First ‘6 Pedalist’ Beverage Cart</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>
								</div>
							</div>
							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/BMM.jpg">
										</div>

										<div class="category-record-title">
											<span>Largest Banner of Mind Maps</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>
							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/CCC.jpg">
										</div>

										<div class="category-record-title">
											<span>Most Number of Cicadas Specimen Collected In A Single Event</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>
							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/CDE.jpg">
										</div>

										<div class="category-record-title">
											<span>Largest Coffee Drinking Event</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>
							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/CTR.jpg">
										</div>

										<div class="category-record-title">
											<span>Tallest Christmas Tree Replica</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>
							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/FPG.jpg">
										</div>

										<div class="category-record-title">
											<span>Largest Fruits & Plants Garden in Recycle Materials</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>
							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/FTPE.jpg">
										</div>

										<div class="category-record-title">
											<span>Longest Distance Motorcycle Ride by A Group</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>
							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/MF.jpg">
										</div>

										<div class="category-record-title">
											<span>Largest Maze Formation</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>
							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/PAC.jpg">
										</div>
										<div class="category-record-title">
											<span>Biggest Painting Assembled by Children</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>
							<div class="grid-item brick">
								<div class="category-record--item">
									<a href="">
										<div class="category-record-image">
											<img src="images/human-achievements/SDE.jpg">
										</div>

										<div class="category-record-title">
											<span>Most Number of Disabled Participants in A Scuba Diving Expedition</span>
										</div>

										<div class="category-record-more-btn">
											<a href="#">More +</a>
										</div>
									</a>

								</div>
							</div>

						</div>
					</div>

					<div class="large-12">
						<div class="load-more">
							<a href="#">Load More</a>
						</div>

						<div class="load-less">
							<a href="#">Load Less</a>
						</div>
					</div>
				</div>

				<div class="large-3 columns record-categories-side-bar">
					<div class="container">
						<div class="categories-side-bar-outer">
							<h5 class="text-uppercase side-bar-title">Breaking Records</h5>
							<div class="categories-side-bar-inner">
								<?php require_once('includes/sidebar-menu.php'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>

		<div class="how-to-be-record-holder">
			<div class="row">
				<div class="how-to-be-record-holder-inner text-center">
					<h3 class="main-title text-uppercase">Are you eligible to become <br/> our record holder?</h3>
						<div class="hw-to-rcd-holder-know-more-btn">
							<a href="#">Know More</a>
						</div>
				</div>
			</div>
		</div>

</body>
<?php require_once('includes/footer.php'); ?>