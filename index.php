<?php 

$pagetitle = "Malaysia Books of Records"; 
require_once('includes/header.php');
require_once('includes/navigation-home.php')
?>

<body>
  <section id="intro-home">
    <div class="grid-container">
      <div class="main--banner">
        <div class="banner--wrap">
          <img src="images/main-banner.jpg">
          <div class="main--banner-desc-wrap">
            <div class="desc-wrap">
              <div class="small-text">
                <h5>> marathon</h5>
                <h3>Runner</h3>
                <p>Join The Malaysian Run 2018</p>
              </div>
            </div>
          </div>
        </div>
        <div class="banner--wrap">
          <img src="images/main-banner.jpg">
        </div>
        <div class="banner--wrap">
          <img src="images/main-banner.jpg">
        </div>
      </div>
    </div>
  </section>



  <!-- ================================== Records ===================================== -->
  <section id="records-home" class="records-home section-inner">
    <div class="container record-container">
      <div class="row">
        <div class="large-12 columns new-rcd-title-outer">
          <div class="section-title">
            <h4>
              <i class="fas fa-ellipsis-v"></i>NEW RECORD</h4>
          </div>

          <div class="see-more-wrap">
            <div class="see-more-btn">
              <a href="#">More
                <i class="fas fa-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="records-wrap--home">
          <div class="large-4 small-6 columns">
            <div class="card-wrap">

              <div class="figure-record--image">
                <div class="record-gallery gallery">
                  <a href="images/swarovski-record.jpg">
                    <img src="images/swarovski-record.jpg">
                  </a>
                </div>
                <div class="category-tag">
                  <h5>Arts</h5>
                </div>
              </div>

              <div class="figure-record--caption read-more">
                <div class="Recordcontent">
                  <h5>First Merry-Go-Round Decorated Using Swarovski Crystal</h5>
                  <p>The fully functional merry-go-round is 8.9metres in height and 8metres in width and weighs about 4tonnes,
                    is part of the Pavilion Kuala Lumpur's Christmas decoration to celebrate the upcoming Christmas festivities.
                    <br> Accepting the certificate from MBR is Dato' Joyce Yap, CEO of Pavilion Kuala Lumpur, with celebrity
                    guest, Godfrey Gao, famous Taiwanese born Canadian actor and Louis Vuitton model</p>
                  <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>

              </div>
            </div>
            <!-- End of Card-wrap -->
          </div>

          <div class="large-4 small-6 columns">
            <div class="card-wrap">
              <div class="figure-record--image">
                <div class="record-gallery gallery">
                  <a href="images/most-expensive-mooncake.jpg">
                    <img src="images/most-expensive-mooncake.jpg">
                  </a>
                </div>
                <div class="category-tag">
                  <h5>Bussiness World</h5>
                </div>
              </div>


              <div class="figure-record--caption read-more">
                <div class="Recordcontent">
                  <h5>Most Expensive Commercial Mooncake</h5>
                  <p>
                    Leong Yin Pastry made it into Malaysia Book of Records for The Most Expensive Commercial Mooncake. Weighing 1.24 kg. Measuring
                    17.5cm and 4cm thickness.
                    <br> Dr.Leong Kok Fai use nutritious ingredients like Ginseng, Cordyceps, Bird Nests and even have 24k Gold
                    Flakes inside the snowskin. Selling at RM3888.</p>
                  <a href="record-detail.php" class="morelink">More +</a>
                </div>
              </div>
            </div>
          </div>
          <div class="large-4 small-6 columns">
            <div class="card-wrap">
              <div class="figure-record--image">
                <div class="record-gallery gallery">
                  <a href="images/entopia.png">
                    <img src="images/entopia.png">
                  </a>
                </div>
                <div class="category-tag">
                  <h5>Nature</h5>
                </div>
              </div>
              <div class="figure-record--caption read-more">
                <div class="Recordcontent">
                  <h5>Biggest Green Wall</h5>
                  <p>Entopia Butterfly House set a new record for the "Biggest Green Wall", as the external facade of the new
                    Entopia building which has been covered almost completely with 88,868 planting pots with 22 species of
                    the tropical plants, measuring 2064.10 square metres and using a hydroponic watering system.
                    <br> Receiving the MBR certificate on behalf of Entopia is Mr David Goh, Chairman of Entopia, and witnessing
                    the presentation is the Yang DiPertua Negeri of Penang, T.Y.T. Tun Dato' Seri Utama (Dr.) Haji Abdul
                    Rahman Bin Haji Abbas.</p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
          </div>

          <div class="large-4 small-6 columns">
            <div class="card-wrap">
              <div class="figure-record--image">
                <div class="record-gallery gallery">
                  <a href="images/swarovski-record.jpg">
                    <img src="images/swarovski-record.jpg">
                  </a>
                </div>

                <div class="category-tag">
                  <h5>Arts</h5>
                </div>
              </div>
              <div class="figure-record--caption read-more">
                <div class="Recordcontent">
                  <h5>First Merry-Go-Round Decorated Using Swarovski Crystal</h5>
                  <p>The fully functional merry-go-round is 8.9metres in height and 8metres in width and weighs about 4tonnes,
                    is part of the Pavilion Kuala Lumpur's Christmas decoration to celebrate the upcoming Christmas festivities.
                    <br> Accepting the certificate from MBR is Dato' Joyce Yap, CEO of Pavilion Kuala Lumpur, with celebrity
                    guest, Godfrey Gao, famous Taiwanese born Canadian actor and Louis Vuitton model</p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>

              </div>
            </div>
          </div>

          <div class="large-4 small-6 columns">
            <div class="card-wrap">
              <div class="figure-record--image">
                <div class="record-gallery gallery">
                  <a href="images/most-expensive-mooncake.jpg">
                    <img src="images/most-expensive-mooncake.jpg">
                  </a>
                </div>

                <div class="category-tag">
                  <h5>Bussiness World</h5>
                </div>
              </div>
              <div class="figure-record--caption read-more" >
                <div class="Recordcontent">
                  <h5>Most Expensive Commercial Mooncake</h5>
                  <p>
                    Leong Yin Pastry made it into Malaysia Book of Records for The Most Expensive Commercial Mooncake. Weighing 1.24 kg. Measuring
                    17.5cm and 4cm thickness.
                    <br> Dr.Leong Kok Fai use nutritious ingredients like Ginseng, Cordyceps, Bird Nests and even have 24k Gold
                    Flakes inside the snowskin. Selling at RM3888.</p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
          </div>
          <div class="large-4 small-6 columns">
            <div class="card-wrap">
              <div class="figure-record--image">
                <div class="record-gallery gallery">
                  <a href="images/entopia.png">
                    <img src="images/entopia.png">
                  </a>
                </div>
                <div class="category-tag">
                  <h5>Nature</h5>
                </div>
              </div>
              <div class="figure-record--caption read-more" >
                <div class="Recordcontent">
                  <h5>Biggest Green Wall</h5>
                  <p>Entopia Butterfly House set a new record for the "Biggest Green Wall", as the external facade of the new
                    Entopia building which has been covered almost completely with 88,868 planting pots with 22 species of
                    the tropical plants, measuring 2064.10 square metres and using a hydroponic watering system.
                    <br> Receiving the MBR certificate on behalf of Entopia is Mr David Goh, Chairman of Entopia, and witnessing
                    the presentation is the Yang DiPertua Negeri of Penang, T.Y.T. Tun Dato' Seri Utama (Dr.) Haji Abdul
                    Rahman Bin Haji Abbas.</p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
          </div>


        </div>
        <!-- End of records-wrap--home -->
      </div>
      <!-- End of Row -->

    </div>
    <!-- End of Records -->
  </section>


  <!-- ================ How to be record holder =========================== -->
  <section class="hw-to-be-record-holder-home" id="hw-to-be-record-holder-home">
    <div class="hw-to-be-rcd-holder-wrapper">
      <div class="row">
        <div class="large-12">
          <div class="how-record-holder-wrap">
            <!-- Background -->
            <div class="hw-rcd-bg" style="background-image: url('images/how-to-be-record-holder-bg.jpg')"></div>

            <!-- Card -->
            <div class="hw-rcd-holder-card">
              <div class="large-4 columns">
                <div class="hw-rcd-card-content">
                  <h3>How To Be Record Holder</h3>
                  <h4>“Are you eligible ?”</h4>
                  <div class="how-to-info-link">
                    <a href="">Guideline</a> |
                    <a href="">SAFETY Precautions</a> |
                    <a href="">Do You Have What It Takes To Be A Record-Holder?</a>
                  </div>
                  <!-- End of info link -->
                  <div class="more-info-btn">
                    <a href="">Get to Know More
                      <i class="fas fa-arrow-right"></i>
                    </a>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <!-- ================ Whats Happening =========================== -->

  <div class="whats-happening-wrapper">
    <div class="whats-happening-bg"></div>
    <div class="row">
      <div class="whats-happening-wrap">
        <div class="large-2 columns">
          <div class="whats-happening-title">
            <div class="whatshappeningheading">
              <h2>What's Happening</h2>
              <hr>
            </div>

            <div class="more-happening">
              <a href="#">More
                <i class="fas fa-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="large-9 columns">
          <div class="row">
            <div class="happening-slider">
              <div class="large-4 columns happening-outer">
                <div class="happening-img-wrapper vertical-middle">
                  <div class="happening-feature-img">
                    <div class="happening-gallery gallery">
                      <a href="images/kl-tower-jump.jpg">
                        <img src="images/kl-tower-jump.jpg">
                      </a>
                      <div class="gallery-hidden">
                        <a href="path-to-image.jpg">Open image 2 (gallery #1)</a>
                      </div>
                    </div>

                  </div>
                </div>

                <div class="happening-feature-caption read-more">
                  <div class="content">
                    <div class="tags">
                      <span>Sports</span>
                    </div>
                    <h6>KL Tower (Menara Kuala Lumpur)</h6>
                    <p>Witness another thrilling action this year. KL Tower (Menara Kuala Lumpur) once again is the host to
                      the KL Tower International Jump Malaysia (KLTIJM) 2017 held from 29th September to 2nd October 2017.
                      The excitement starts at the Sky Deck of KL Tower, located at Tower Head 5 (TH05) at the height of
                      312 metres above sea level. Watch BASE jumpers perform spectaculars jump and other daring manoeuvres.
                      This sporting event was first introduced on 3rd October 1999 in conjunction with KL Tower’s 3rd anniversary
                      celebration. Since then, it has become an annual event for KL Tower and today, 2nd October 2017, KL
                      Tower (Menara Kuala Lumpur Sdn Bhd) has marked another 3 National records in the Malaysia Book of Records
                      for 1) HIGHEST BASE JUMP PLATFORM (312 metres above sea level) 2) FIRST ROPE BASE JUMP CHALLENGE 3)
                      MOST NUMBER OF PARTICIPANTS IN A BASE JUMP CHALLENGE (105 base jumpers) Congratulations for the awesome
                      achievement
                    </p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                  </div>
                </div>
              </div>

              <div class="large-4 columns happening-outer">

                <div class="happening-img-wrapper vertical-middle">
                  <div class="happening-feature-img">
                    <div class="happening-gallery gallery">
                      <a href="images/most-number-of-online-merchant.jpg">
                        <img src="images/most-number-of-online-merchant.jpg">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="happening-feature-caption read-more">
                  <div class="content">
                    <div class="tags">
                      <span>Arts</span>
                    </div>
                    <h6>The Most Number of Online Merchant in Online Sales Event</h6>
                    <p>National ICT Association Malaysia or Pikom set a new record for The Most Number of Online Merchant in
                      Online Sales Event. Number registered as of Record time is 1007 merchants. Remember to go online and
                      shop in #MyCyberSale2017. Sales period from 9th to 13th Oct. Guest of honour for the event was graced
                      by Datuk Yasmin Mahmood CEO My MDEC. https://mycybersale.my/</p>
                      <a href="record.php" target="_blank" class="morelink">More +</a>
                  </div>
                </div>
              </div>

              <div class="large-4 columns happening-outer">
                <div class="happening-img-wrapper vertical-middle">
                  <div class="happening-feature-img">
                    <div class="happening-gallery gallery">
                      <a href="images/first-tilling-academy.jpg">
                        <img src="images/first-tilling-academy.jpg">
                      </a>
                    </div>
                  </div>
                </div>

                <div class="happening-feature-caption read-more">
                  <div class="content">
                    <div class="tags">
                      <span>Arts</span>
                    </div>
                    <h6>First Tiling Academy</h6>
                    <p>MBR is very proud to certify this sustainable CSR program by Feruni Tiling Academy as 'First Tiling Academy'
                      in Malaysia. Feruni Tiling Academy (FTA) holds its first ever graduation ceremony on the 4th October
                      2017 to commemorate the first batch of students who have passed the intensive training programme with
                      flying colours. FTA was established by Feruni Ceramiche as a CSR initiative with the aim to uplift
                      the lives of the community and transform the tiling industry. This is a collaborative effort together
                      with Construction Industry Development Board (CIDB) Holdings Sdn Bhd, Akademi Binaan Malaysia (ABM),
                      Mapei Malaysia and Pavire (M) Sdn Bhd. It is a six-month intensive training programme focussing on
                      tile installation methodology and best practices. Students who have successfully completed their training
                      will be known as “Tilistas” (professional tilers), where they will receive certification endorsed by
                      CIDB Holdings Sdn Bhd through Akademi Binaan Malaysia.</p>
                      <a href="record.php" target="_blank" class="morelink">More +</a>
                  </div>
                </div>
              </div>

              <div class="large-4 columns happening-outer">
                <div class="happening-img-wrapper vertical-middle">
                  <div class="happening-feature-img">
                    <div class="happening-gallery gallery">
                      <a href="images/youngest-parliment.jpg">
                        <img src="images/youngest-parliment.jpg">
                      </a>
                    </div>

                  </div>
                </div>
                <div class="happening-feature-caption read-more">
                  <div class="content">
                    <div class="tags">
                      <span>Arts</span>
                    </div>
                    <h6>Youngest Member of Parliament</h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- End of whats-happening-wrapper -->

  <!-- =========================== Events ================================= -->
  <section class="event-home section-inner" id="event-home">
    <div class="event-wrapper container">
      <div class="row">
        <div class="large-12 columns event-title-outer">
          <div class="section-title">
            <h4>
              <i class="fas fa-ellipsis-v"></i>EVENTS</h4>
          </div>

          <div class="see-more-wrap">
            <div class="see-more-btn">
              <a href="#">More
                <i class="fas fa-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div class="large-12">
        <div class="event-wrap">
          <div class="row large-up-4 medium-up-2 small-up-2">
            <div class="columns">
              <div class="event-feature-img">
                <div class="event-gallery gallery">
                  <a href="images/akademifantasia.jpg">
                    <img src="images/akademifantasia.jpg">
                  </a>
                </div>

              </div>
              <div class="event-feature-caption read-more">
                <div class="content">
                  <div class="tags">
                    <span>Buildings and Structures</span>
                  </div>
                  <h6>Longest Running Singing Reality Television Show</h6>
                  <p>The first season of Akademi Fantasia (AF) premiered on June 9, 2003 on the Astro Ria television channel.
                    12 contestants were selected and went through the studies in the academy consist of vocal, stage performance,
                    dance and motivational aspects. This year, AF once again marked another nation record as the "Longest
                    Running Singing Reality Television Show". Throughout this 14th seasons, the content of the programme
                    was not only a weekly concert taking place every weekend. It also involves AF diary, where the contestants
                    were quarantine in a secret house and all their activities were monitored by set of cameras. AF Megastar
                    assembled all AF Stars from Season 1- Season 13 in way to decide who is the best among the best! </p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
            <div class="columns">
              <div class="event-feature-img">
                <div class="event-gallery gallery">
                  <a href="images/fish-fry-restocking-event.jpg">
                    <img src="images/fish-fry-restocking-event.jpg">
                  </a>
                </div>
              </div>
              <div class="event-feature-caption read-more">
                <div class="content">
                  <div class="tags">
                    <span>Nature</span>
                  </div>
                  <h6>Largest Simultaneous Fish Fry Restocking Event</h6>
                  <p>MBR was at Putrajaya Lake today to present the MBR certificate to the Jabatan Perikanan Malaysia (Department
                    of Fisheries Malaysia) for the “Largest Simultaneous Fish Fry Restocking Event”. The record attempt was
                    part of a simultaneous fish fry release exercise totalling 482,242 fish fry of various species throughout
                    the nation’s lakes and rivers in 14 States as part of the department’s National Fishermen Wave program
                    to enhance the fish population and in turn improve the standard of living for the nation’s fishermen.
                    The certificate presentation was made to the Director-General of Malaysia Fisheries Department, in the
                    presence of the Deputy Minister of Agriculture & Agro-Based Industry</p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
            <div class="columns">
              <div class="event-feature-img">
                <div class="event-gallery gallery">
                  <a href="images/longest-mooncake-line.jpg">
                    <img src="images/longest-mooncake-line.jpg">
                  </a>
                </div>

              </div>
              <div class="event-feature-caption read-more">
                <div class="content">
                  <div class="tags">
                    <span>Arts</span>
                  </div>
                  <h6>Longest Mooncake Line</h6>
                  <p>MBR is in the small town of Bintulu to present to the Federation of Registered Chinese Association Bintulu
                    Divison (FORCA) the MBR certificate for the "Longest Mooncake Line". The current record of 888 metres
                    was achieved by FORCA after they broke the old record of 710.32 metres, also set in Bintulu back in 2011.
                    In total, there are 2914 block pieces of mooncake that formed the record, which are then sold to the
                    public and all proceeds will then be channelled for donation to charities. Witnessing the MBR certificate
                    to FORCA was Datuk Sri Dr Stephen Rundi, the Minister of Utilities of Sarawak</p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
            <div class="columns">
              <div class="event-feature-img">
                <div class="event-gallery gallery">
                  <a href="images/biggest-mosaic-made-of-crayons.jpg">
                    <img src="images/biggest-mosaic-made-of-crayons.jpg">
                  </a>
                </div>
              </div>

              <div class="event-feature-caption read-more">
                <div class="content">
                  <div class="tags">
                    <span>Arts</span>
                  </div>
                  <h6>Biggest Mosaic Made of Crayons</h6>
                  <p>
                    Animals are ‘alive’ and seen ‘roaming free’ on a giant safari mosaic made of recycled crayons in Cheras Leisure Mall! On
                    August 2, 2017, in conjunction with its Animal Tales fundraising program, Cheras Leisure Mall in partnership
                    with Crayola Malaysia, successfully created the ‘Biggest Mosaic Made of Crayons’ for the Malaysia Book
                    of Records– all for the monumental cause of increasing awareness on animal welfare issues. Artistically
                    constructed at Level 2 Cravings Lane, Cheras Leisure Mall, shoppers and visitors will be captivated by
                    a giant mosaic depicting a harmonious group of animals living in the wild, measuring 10.28 metres in
                    length and 6.97 metres in width. Brought to life with 3 tonnes of recycled crayon pieces and 2,270 mini
                    mosaics, the masterpiece is a result of a community engagement initiative, whereby more than 2,000 participants
                    of all walks of life – from as young as a 4-year old child to an 80-year old participant – who came together
                    through a common passion for animal welfare issues. Held from 15 – 31 July 2017, participants had the
                    opportunity to build mini mosaics using recycled Crayola crayon pieces with a participation fee of RM10
                    each. The mini mosaics were then combined and repurposed to create the ‘Biggest Mosaic Made of Crayons’,
                    which became an iconic showpiece at Cheras Leisure Mall to raise public awareness about animal protection
                    and conservation.

                  </p>
                  <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
            <div class="columns">
              <div class="event-feature-img">
                <div class="event-gallery gallery">
                  <a href="images/ponytails-campaigns.jpg">
                    <img src="images/ponytails-campaigns.jpg">
                  </a>
                </div>
              </div>
              <div class="event-feature-caption read-more">
                <div class="content">
                  <div class="tags">
                    <span>Arts and Entertainment</span>
                  </div>
                  <h6>Largest Participation in a 'Ponytail' Donation Campaign</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>
                  <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
            <div class="large-3 medium-4 small-6 columns">
              <div class="event-feature-img">
                <div class="event-gallery gallery">
                  <a href="images/most-palm-print-on-wall.jpg">
                    <img src="images/most-palm-print-on-wall.jpg">
                  </a>
                </div>

              </div>
              <div class="event-feature-caption read-more">
                <div class="content">
                  <div class="tags">
                    <span>Arts</span>
                  </div>
                  <h6>Most Number of Palm Prints Stamped on a Wall Mural</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>
                  <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
            <div class="columns">
              <div class="event-feature-img">
                <div class="event-gallery gallery">
                  <a href="images/gold-diving.jpg">
                    <img src="images/gold-diving.jpg">
                  </a>
                </div>
              </div>
              <div class="event-feature-caption read-more">
                <div class="content">
                  <div class="tags">
                    <span>Arts</span>
                  </div>
                  <h6>First To Win Diving Gold Medal In World Aquatics Championships</h6>
                  <p>MBR is proud to recognise and present to the latest national sporting sensation, Cheong Jun Hoong, the
                    MBR certificate for "First To Win Diving Gold Medal In World Aquatics Championships". This record was
                    to acknowledge her achievement in the latest FINA World Championships 2017 in Budapest, Hungary. Cheong
                    Jun Hoong is also the second Malaysian athlete to win World Championships in an Olympic sports. The certificate
                    presentation was done by the MBR Deputy General Manager at Kuala Lumpur International Airport with YB
                    Brig Jen Khairy Jamaluddin (Minister of Youth and Sports), representatives from the sporting bodies and
                    members of the media in attendance. Congratulations Cheong Jun Hoong</p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>
            <div class="columns">
              <div class="event-feature-img">
                <div class="event-gallery gallery">
                  <a href="images/largest-cultural-parade.jpg">
                    <img src="images/largest-cultural-parade.jpg">
                  </a>
                </div>
              </div>
              <div class="event-feature-caption read-more">
                <div class="content">
                  <div class="tags">
                    <span>Arts</span>
                  </div>
                  <h6>Largest Cultural Parade</h6>
                  <p>In Ipoh this morning to present to the Perak Chamber of Commerce & Industry the MBR certificate for the
                    "Largest Cultural Parade" for organising about 54 local organisations and famous Perak brands together
                    with a total of 2296 participants to showcase the lifestyle and culture of the State of Perak by way
                    of a cultural parade and floats in downtown Ipoh. In attendance to witness this community engagement
                    event is the Menteri Besar of Perak, YB Dato' Seri Diraja Dr. Zambry Bin Abd Kadir</p>
                    <a href="record.php" target="_blank" class="morelink">More +</a>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="event-bottom-bg"></div>
  </section>



  <?php require_once('includes/modal.php');?>
  <?php require_once('includes/footer.php');?>

  <script>

    $(".menu-icon").click(function () {
      $(".top-bar").addClass('expanded');
    });


      $('.main--banner').slick({
        dots: true,
        infinite: false,
        speed: 300,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        mobileFirst: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });

      $('.records-wrap--home').slick({
        dots: false,
        infinite: false,
        speed: 300,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 750,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });

      $('.happening-slider').slick({
        dots: false,
        infinite: false,
        speed: 300,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 750,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });

      $(document).ready(function () {
        // Configure/customize these variables.
        var showChar = 50; // How many characters are shown by default
        var ellipsestext = "...";
        var moretext = "Show more >";
        var lesstext = "Show less";


        $('.content p').each(function () {
          var content = $(this).html();

          if (content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext +
              '&nbsp;</span>';

            $(this).html(html);
          }

        });

      });

      $(document).ready(function () {
        // Configure/customize these variables.
        var showCharRecords = 60; // How many characters are shown by default
        var ellipsestextRecords = "...";
        var moretextRecords = "Show more >";
        var lesstextRecords = "Show less";


        $('.records-wrap--home .Recordcontent p').each(function () {
          var contentRecords = $(this).html();

          if (contentRecords.length > showCharRecords) {

            var g = contentRecords.substr(0, showCharRecords);
            var m = contentRecords.substr(showCharRecords, contentRecords.length - showCharRecords);

            var htmlRecord = g + '<span class="moreellipses">' + ellipsestextRecords +
              '&nbsp;</span>';

            $(this).html(htmlRecord);
          }

        });

      });

      $('.gallery').each(function () { // the containers for all your galleries
        $(this).magnificPopup({
          delegate: 'a', // the selector for gallery item
          type: 'image',
          gallery: {
            enabled: true
          }
        });
      });
  </script>
</body>

</html>