<?php 
$pagetitle = "Malaysia Books of Records - Record"; 
require_once('includes/header.php');
require_once('includes/navigation-main.php')
?>

<!-- Record-banner -->
<div class="header-banner">
    <div class="header-banner-wrap">
        <div class="row">
            <div class="large-12">

                <div class="header-banner-image record-header" style="background-image: url('images/record-banner.jpg')">
                    <div class="large-5 small-8 header-banner-title--container">
                        <div class="header-banner-title--wrap">
                            <div class="header-banner-title">
                                <h3 class="page--title">Records</h3>
                                <div class="page--subtitle">
                                    <p>Intriguing, impressive records keep pouring in, the new beginning embarked upon is an
                                        unending journey</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Records Detail -->
<div class="container page-container">
    <div class="row small-up-1 medium-up-2 large-up-3 records-categories-wrap">
        <div class="columns column-block">
            <a href="human-achievements.php">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/EOCM.jpg">
                        </div>

                        <a href="human-achievements.php">
                            <div class="records-desc-wrap">
                                <h4>Human Achievement</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>

        <div class="columns column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/BDD.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Human Beings</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>


        <div class="column column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/RWDG.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Human World</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>
        <div class="column column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/WR.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Buildings & Structures</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>
        <div class="column column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/LT.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Transportation</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>
        <div class="column column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/NGE.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Bussiness World</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>
        <div class="column column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/WLSC.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Sports & Games</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>
        <div class="column column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/LEGOLAND.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Arts & Entertainments</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>
        <div class="column column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/OTBS.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Science & Technology</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>
        <div class="column column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/panda.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Living World</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>
        <div class="column column-block">
            <a href="">
                <div class="records-category-wrapper">
                    <div class="records-category">
                        <div class="records-image-wrap">
                            <img src="images/records/MERC.jpg">
                        </div>

                        <a href="">
                            <div class="records-desc-wrap">
                                <h4>Nature</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- records-category-wrapper -->
            </a>
        </div>
    </div>
</div>

<div class="how-to-be-record-holder">
    <div class="row">
        <div class="how-to-be-record-holder-inner text-center">
            <h3 class="main-title text-uppercase">Are you eligible to become
                <br/> our record holder?</h3>
            <div class="hw-to-rcd-holder-know-more-btn">
                <a href="#">Know More</a>
            </div>
        </div>
    </div>
</div>

<?php require_once('includes/footer.php')
?>