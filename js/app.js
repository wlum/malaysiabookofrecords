$(document).foundation()

/*========================================================== 
PRELOADER JS
========================================================== */	
$(window).on('load', function() {
    $('.preloader-bounce').fadeOut();
    $('.preloader').delay(200).fadeOut('slow');
});