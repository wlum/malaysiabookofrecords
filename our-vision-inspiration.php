<?php 
$pagetitle = "Malaysia Books of Records - About Us"; 
require_once('includes/header.php');
require_once('includes/navigation-main.php')
?>


<!-- Record-banner -->
<div class="page--about">
    <div class="header-banner">
        <div class="header-banner-wrap">
            <div class="row">
                <div class="large-12">
                    
                    <div class="header-banner-image" style="background-image: url('images/36611252_2096038483943986_6207664632618811392_n.jpg')">
                        <div class="large-5 small-8 header-banner-title--container">
                            <div class="header-banner-title--wrap">
                                <div class="header-banner-title">
                                    <h3 class="page--title">About Us</h3>
                                    <h5 class="page--subtitle">Our Vision and Inspiration</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container page-container vision-inspiration-page-container">
    <div class="row vision-inspiration-section">       
        <div class="large-12 columns">
            <div class="about--vision-wrap">
                <div class="about--award-night-title">
                    <h3>Vision & Inspiration</h3>
                </div>

                <div class="about--vision">
                    <p>It has been a remarkable period for the Malaysia Book of Records which has continuously showcased prime
                        examples of the trials, tribulations and triumphs of Malaysians. From its humble beginnings on July
                        11, 1995, the Malaysia Book of Records has served as a platform for Malaysians to be the best of
                        the best by rising to greater heights of glory, achievements and excellence. In so doing, these Malaysians
                        have set the benchmark for others to reach, if not breach. Rising above themselves is the keyword
                        inherent in these record-breakers, as they dig deep into their reserves of energy and ability to
                        produce extraordinary efforts that will forever be remembered by fellow Malaysians. Like a pebble
                        dropped into the sea that causes a ripple of water to spread far and wide, the Malaysia Book of Records
                        has helped to turn on the faucets of enthusiasm, determination and perseverance that had somewhat
                        remained untapped in Malaysians. This overflowed into a torrent of records, feats and achievements
                        accomplished by Malaysians that bore testament to the 'Malaysia Boleh' spirit and served as tools
                        in the nation-building process while propelling towards Wawasan 2020. Since its inception, the Malaysia
                        Book of Records - being a non-profit-making entity - has gone through tumultuous periods and trying
                        times of its own. But with will, hope and strength acting as its bulwarks, it has always emerged
                        unscathed on each occasion. As the nation's record keeper, it staunchly practises the very principles
                        and attributes of determination and perseverance as demonstrated by the featured achievers in its
                        pages. The Malaysia Book of Records itself has set a milestone with the publication of its first-ever
                        'Edisi Dwibahasa (Bahasa Malaysia - English)', a bilingual version that will reach out to an even
                        more expansive audience, in hopes that more readers are bound to be inspired and fired up to scale
                        greater heights. As long as there are new Malaysian record-breakers falling under the spotlight,
                        the work of Malaysia Book of Records will always remain unfinished. It will continue to emphasise,
                        encourage and support attempts at setting and breaking records.
                    </p>
                    
                    <blockquote>THE MALAYSIA BOOK OF RECORDS ' SETTING A SOLID FOUNDATION FOR THE FUTURE GENERATIONS OF MALAYSIANS '</blockquote>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mbr-mission-vision">
        <div class="large-6 columns">
            <div class="row">
                <div class="large-12 columns">
                    <div class="about--mbr-vision-wrap">
                        <div class="about--mbr-vision-title">
                            <h3>MBR Vision</h3>
                        </div>
                    </div>
                </div>
                
                <div class="large-12 columns">
                    <div class="about--mbr-vision-wrap">
                        <div class="about--mbr-vision-content">
                            <p>Putting national interest above everything else was the driving factor that led to the setting
                                up of The Malaysia Book of Records.
                                <br/> The need to accord recognition and reverence to national achievers and record breakers,
                                gave birth to a publication that would document not only for posterity purposes, but also
                                to serve as a source of inspiration for others to try and earn honour and pride for the country.
                                <br/> With the country's interest at heart, the founder fervently believed a compendium featuring
                                the feats and achievements accomplished by Malaysians, should see the light of day. This
                                allows achievers to share their records with not only their families and friends, but also
                                with the nation and the world.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="large-6 columns">
            <div class="row">
                <div class="large-12 columns">
                    <div class="about--mbr-vision-wrap">
                        <div class="about--mbr-vision-title">
                            <h3>MBR Mission</h3>
                        </div>
                    </div>
                </div>
                
                <div class="large-12 columns">
                    <div class="about--mbr-vision-wrap">
                        <div class="about--mbr-vision-content">
                            <p>
                                <ul>
                                    <li>To record and recognise human feats & achievements</li>
                                    <li>To support and encourage more human feats and achievements for worldwide recognition</li>
                                    <li>To instil the spirit of excellence among Malaysians</li>
                                    <li>To complement our country's "1Malaysia" campaign</li>
                                    <li>To provide interesting information and to enrich general knowledge of the public & tourists
                                        on Malaysians' achievements</li>
                                        <li>To store and exhibit outstanding achievements through the setting up of MBR " Museum
                                            Of Achievers"</li>
                                        </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
        
        
    </div>
    <?php  require_once('includes/footer.php');?>