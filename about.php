<?php 
$pagetitle = "Malaysia Books of Records - About Us"; 
require_once('includes/header.php');
require_once('includes/navigation-main.php')
?>

<!-- Record-banner -->
<div class="page--about">
    <div class="header-banner">
        <div class="header-banner-wrap">
            <div class="row">
                <div class="large-12">
                    
                    <div class="header-banner-image" style="background-image: url('images/36611252_2096038483943986_6207664632618811392_n.jpg')">
                        <div class="large-5 small-8 header-banner-title--container">
                            <div class="header-banner-title--wrap">
                                <div class="header-banner-title">
                                    <h3 class="page--title">About Malaysia Book of Records</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    <div class="container page-container">
        <div class="brief-background-container">        
            <div class="about--background-wrap">
                <div class="about--background-title">
                    <h3>Background of Malaysia Book Of Records </h3>                
                </div>
                
                <div class="about--background-content">
                    <p>Back when the government called upon the private sector to play an active role in Nation Building
                        towards Vision 2020, Malaysia Book of Records was created to be in line with this ambition.
                        <br/> MBR act as an official body that recognises record-holders, record-breakers and record-creators
                        in the country.
                        <br/> Upon confirmation of their feats, achievements and creations, MBR issues certificates to
                        them.
                    </p>
                </div>
            </div>
            
            
            <div class="row mbr-info">
                <div class="large-6 columns">
                    <div class="about--project-wrap">
                        <div class="about--project-title vertical-middle">
                            <h4>A Project that inspires the Strive for Excellence </h4>
                        </div>
                        
                        <div class="about--project-content">
                            <p>In order to fulfil the goals and purposes to acknowledge, recognise and keep track of all achievements,
                                creations and human feats, appropriate stages were set, such as the compilation of all records
                                for the publication of The Malaysia Book of Records and the production of The Malaysia Book
                                of Records TV Series.
                                <br/> The Malaysia Book of Records' First Edition was successfully launched on December 9, 1998,
                                unveiling the the best of Malaysians and Malaysia in one book for the first time in Malaysian
                                history.
                                <br/> The Malaysia Book of Records' weekly TV series which outset on October 6, 1996 on RTM TV2
                                also serves as the most powerful medium in providing the acknowledgement and recognition
                                to all Malaysians who in one way or another, had taken the extra miles to make our country
                                and her people proud.
                            </p>
                        </div>
                    </div> 
                </div>
                
                <div class="large-6 columns">
                    <div class="about--how-it-started-wrap">
                        <div class="about--how-it-started-title vertical-middle">
                            <h4> How it Started</h4>
                        </div>
                        <div class="about--how-it-started-content">
                            <p>This unorthodox Malaysian idea formed in 1990 when Mr. Danny Ooi, the Founder of The Malaysia
                                Book of Records, stumbled upon a few questions regarding extraordinary feats set by Malaysians.
                                <br/> He recalled seeing a man cycling for days at the Merdeka Stadium, trying to set a world
                                record and, another individual, who travelled from state to state by walking. The letter
                                was hoping his attempt could be entered in the Guinness Book of Records.
                                <br/> Realising that none of these feats and attempts could be recorded or kept, Mr Ooi felt it
                                was time that proper recognition should be given to such aspiration, effort and determination
                                exhibited by Malaysians.
                                <br/> Therefore, MBR is set to motivate and inspire Malaysians to excel in various fields to the
                                extent of surpassing the world's standard. This also serves as a tool in highlighting Malaysia
                                to the world.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        
        <div class="mbr-award-outer row">                 
            <div class="large-12 columns">
                <div class="about--award-night-title">
                    <h3>MBR Awards Night at National and State Level</h3>
                    <hr>
                </div> 
            </div>             
            
            <div class="large-6 columns">
                <div class="about--award-night-wrap">
                    
                    <div class="about--award-night-content">
                        <p>Starting December 1998, The Malaysia Book of Records had successfully organised MBR Awards Night
                            at the national level and two states' level, namely Sabah and Melaka.
                            <br/> With true determination and ambitions, The Malaysia Book of Records has been and will always
                            be there for all Malaysians who are ready to create, achieve and exhibit the spirit of "Malaysia
                            Boleh".
                        </p>
                    </div>
                </div>
            </div>
            
            
            <div class="large-6 columns">
                <div class="about--award-night-image-wrap">
                    <div class="about--award-night-image">
                        <img src="images/35188964_2077160982498403_5691907801320783872_o.jpg">
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="container page-container vision-inspiration-page-container">
            <div class="row vision-inspiration-section">       
                <div class="large-12 columns">
                    <div class="about--vision-wrap">
                        <div class="about--award-night-title">
                            <h3>Vision & Inspiration</h3>
                        </div>
                        
                        <div class="about--vision">
                            <p>It has been a remarkable period for the Malaysia Book of Records which has continuously showcased prime
                                examples of the trials, tribulations and triumphs of Malaysians. From its humble beginnings on July
                                11, 1995, the Malaysia Book of Records has served as a platform for Malaysians to be the best of
                                the best by rising to greater heights of glory, achievements and excellence. In so doing, these Malaysians
                                have set the benchmark for others to reach, if not breach. Rising above themselves is the keyword
                                inherent in these record-breakers, as they dig deep into their reserves of energy and ability to
                                produce extraordinary efforts that will forever be remembered by fellow Malaysians. Like a pebble
                                dropped into the sea that causes a ripple of water to spread far and wide, the Malaysia Book of Records
                                has helped to turn on the faucets of enthusiasm, determination and perseverance that had somewhat
                                remained untapped in Malaysians. This overflowed into a torrent of records, feats and achievements
                                accomplished by Malaysians that bore testament to the 'Malaysia Boleh' spirit and served as tools
                                in the nation-building process while propelling towards Wawasan 2020. Since its inception, the Malaysia
                                Book of Records - being a non-profit-making entity - has gone through tumultuous periods and trying
                                times of its own. But with will, hope and strength acting as its bulwarks, it has always emerged
                                unscathed on each occasion. As the nation's record keeper, it staunchly practises the very principles
                                and attributes of determination and perseverance as demonstrated by the featured achievers in its
                                pages. The Malaysia Book of Records itself has set a milestone with the publication of its first-ever
                                'Edisi Dwibahasa (Bahasa Malaysia - English)', a bilingual version that will reach out to an even
                                more expansive audience, in hopes that more readers are bound to be inspired and fired up to scale
                                greater heights. As long as there are new Malaysian record-breakers falling under the spotlight,
                                the work of Malaysia Book of Records will always remain unfinished. It will continue to emphasise,
                                encourage and support attempts at setting and breaking records.
                            </p>
                            
                            <blockquote>THE MALAYSIA BOOK OF RECORDS ' SETTING A SOLID FOUNDATION FOR THE FUTURE GENERATIONS OF MALAYSIANS '</blockquote>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row mbr-mission-vision">
                <div class="large-6 columns">
                    <div class="row">
                        <div class="large-12 columns">
                            <div class="about--mbr-vision-wrap">
                                <div class="about--mbr-vision-title">
                                    <h3>MBR Vision</h3>
                                </div>
                            </div>
                        </div>
                        
                        <div class="large-12 columns">
                            <div class="about--mbr-vision-wrap">
                                <div class="about--mbr-vision-content">
                                    <p>Putting national interest above everything else was the driving factor that led to the setting
                                        up of The Malaysia Book of Records.
                                        <br/> The need to accord recognition and reverence to national achievers and record breakers,
                                        gave birth to a publication that would document not only for posterity purposes, but also
                                        to serve as a source of inspiration for others to try and earn honour and pride for the country.
                                        <br/> With the country's interest at heart, the founder fervently believed a compendium featuring
                                        the feats and achievements accomplished by Malaysians, should see the light of day. This
                                        allows achievers to share their records with not only their families and friends, but also
                                        with the nation and the world.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="large-6 columns">
                    <div class="row">
                        <div class="large-12 columns">
                            <div class="about--mbr-vision-wrap">
                                <div class="about--mbr-vision-title">
                                    <h3>MBR Mission</h3>
                                </div>
                            </div>
                        </div>
                        
                        <div class="large-12 columns">
                            <div class="about--mbr-vision-wrap">
                                <div class="about--mbr-vision-content">
                                    <p>
                                        <ul>
                                            <li>To record and recognise human feats & achievements</li>
                                            <li>To support and encourage more human feats and achievements for worldwide recognition</li>
                                            <li>To instil the spirit of excellence among Malaysians</li>
                                            <li>To complement our country's "1Malaysia" campaign</li>
                                            <li>To provide interesting information and to enrich general knowledge of the public & tourists
                                                on Malaysians' achievements</li>
                                                <li>To store and exhibit outstanding achievements through the setting up of MBR " Museum
                                                    Of Achievers"</li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                
                
            </div>
        </div>       
        
        <?php  require_once('includes/footer.php');?>
        