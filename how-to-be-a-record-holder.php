<?php 
$pagetitle = "Malaysia Books of Records - How To Be A Record Holder"; 
require_once('includes/header.php');
require_once('includes/navigation-main.php')
?>

<div class="page--how-to-be-record-holder">
    <div class="header-banner page--how-to-be-record-holder-header-banner">
        <div class="header-banner-wrap">
            <div class="row">
                <div class="large-12">
                    <div class="header-banner-image" style="background-image: url('images/how-to-be-a-rcd-holder-bg.jpg')"></div>
                    <div class="large-5 small-8 header-banner-title--container">
                        <div class="header-banner-title--wrap">
                            <div class="header-banner-title">
                                <h3 class="page--title">How To Be A Record Holder</h3>
                                <div class="page--subtitle">
                                    <p>Everyone is qualified. Everyone can set or break a record, as an individual or as part of a team.
                                        <br/><br/>We accept original and creative ideas in as many ways possible. What we are looking for, however, are attempts that are interesting, involving some degree of skill, reasonably safe and have the potential to attract others eager to surpass your feats. 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
        <!-- End of Header Banner Wrap-->
        
        <div class="row collapse how-info-container">
            <div class="large-3 columns">
                <ul class="vertical tabs how-info-tabs" data-responsive-accordion-tabs="tabs medium-accordion large-tabs" id="example-tabs">
                    <li class="tabs-title is-active"><a href="#guideline-panel" aria-selected="true"><i class="far fa-file-alt"></i>Guidelines</a></li>
                    <li class="tabs-title"><a href="#precautions-panel"><i class="fas fa-exclamation"></i>Safety Precautions</a></li>
                    <li class="tabs-title"><a href="#do-you-have-what-it-takes-panel"><i class="fas fa-info-circle"></i>Do You Have What It Takes To Be A Record-Holder?</a></li>
                </ul>
            </div>
            
            <div class="large-9 columns how-info-content">
                <div class="tabs-content" data-tabs-content="example-tabs">
                    <div class="tabs-panel is-active" id="guideline-panel">
                        <p>The responsibility of proving a record is on the claimant. All entries submitted must be validated by adequate documentation, without which the record will not be accepted.</p>
                    </div>
                    <div class="tabs-panel" id="precautions-panel">
                        <p>The Malaysia Book of Records emphasises all safety precautions must be strictly adhered to, and medical services must be available at all times. All relevant paperwork and approval must be obtained from the 
                            respective regulatory authorities to ensure the legality and smooth running of any event.
                            The Malaysia Book of Records cannot be held responsible for any (potential) liability or injury whatsoever arising out of such attempts, whether to the claimant or third party.
                        </p>
                    </div>
                    <div class="tabs-panel" id="do-you-have-what-it-takes-panel">
                        <p>The Malaysia Book of Records features individuals and groups that have accomplished extraordinary feats. If you think you have what it takes to create or break a record, then you are well on your way to stamping your name in history.<br/>
                            <br/>If you have a particular talent, passion or even a one-of-a-kind collection, give us a heads up. Or, if you are fresh out of ideas on how to create or break records, just browse through these pages and be inspired, amazed or just plain dumbfounded.<br/>
                            <br/>These include photographs, which are a must; independent corroboration in the form of local or national newspaper cuttings; and signed statements of authentication by two or more independent persons of some standing in the community, including contact addresses and telephone numbers, and confirming all our rules have been adhered to.<br/>
                            <br/>Potential record-breakers are required to write in early to notify us of their proposals or to obtain the current status of each record before any attempt. This is to ensure proper rules and procedures are followed, and to avoid disqualification on technicalities.<br/>
                            <br/>While attempting to break an existing record in most of the Human Achievements categories, The Malaysia Book of Records has specific guidelines to ensure the consistency of precedents rules already established.<br/>
                            <br/>The Malaysia Book of Records strives to be completely authentic. There may be the rare instances where an earlier record may become known after the book is published. In such cases, amendments and updates will be carried only in the next edition.<br/>
                            <br/>For reasons of space or others, a confirmed entry may be dropped at the time of going to print, as any record may subsequently be broken. The publisher is not bound to inform the claimant of the change in status.<br/>
                            <br/>The Malaysia Book of Records reserves the right to feature and to publish any record attempts, and the use of record details, photographs and audio visuals for publicity purposes.
                        </p>
                    </div>
                </div>
            </div>
        </div>

    <!-- End of header-banner -->
</div>
<!-- End of page--how-to-be-record-holder -->
<?php require_once('includes/footer.php') ?>