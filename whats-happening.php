<?php 
$pagetitle = "Malaysia Books of Records - What's Happening"; 
require_once('includes/header.php');
require_once('includes/navigation-main.php')
?>

<div class="page--how-to-be-record-holder">
    <div class="header-banner page--how-to-be-record-holder-header-banner">
        <div class="header-banner-wrap">
            <div class="row">
                <div class="large-12">
                    <div class="header-banner-image" style="background-image: url('images/35188964_2077160982498403_5691907801320783872_o.jpg')"></div>
                    <div class="large-5 small-8 header-banner-title--container">
                        <div class="header-banner-title--wrap">
                            <div class="header-banner-title">
                                <h3 class="page--title">What's Happening</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require_once('includes/footer.php') ?>