<!-- Navigation -->
<div data-sticky-container class="home-nav">
  <div data-sticky data-options="marginTop:0;">

    <div class="title-bar" data-responsive-toggle="menu-home" data-hide-for="large">
      <button class="menu-icon" type="button" data-toggle="menu-home"></button>
      <div class="title-bar-title">
        <img src="images/malaysia-book-of-records-logo.png">
      </div>
    </div>

    <div class="top-bar row" id="menu-home">
      <div class="logo--wrap">
        <a href="index.php">
          <img src="images/malaysia-book-of-records-logo.png">
        </a>
      </div>

      <div class="logo--sticky">
        <a href="index.php">
          <img src="images/malaysia-book-of-records-logo-dark.png">
        </a>
      </div>

      <ul class="nav-right vertical medium-horizontal dropdown menu" data-responsive-menu="accordion medium-dropdown">
        <li <?php if (basename($_SERVER[ 'PHP_SELF'])=='about.php' ) { echo "class='active'"; }?>>
          <a href="about.php">About</a>
        </li>
        <li <?php if (basename($_SERVER[ 'PHP_SELF'])=='record.php' ) { echo "class='active'"; }?>>
          <a href="record.php">Record</a>
        </li>
        <li <?php if (basename($_SERVER[ 'PHP_SELF'])=='#' ) { echo "class='active'"; }?>>
          <a href="#">How to be record holder</a>
        </li>
        <li <?php if (basename($_SERVER[ 'PHP_SELF'])=='#' ) { echo "class='active'"; }?>>
          <a href="#">Whats Happening</a>
        </li>
        <li <?php if (basename($_SERVER[ 'PHP_SELF'])=='#' ) { echo "class='active'"; }?>>
          <a href="#">Contact</a>
        </li>
        <li class="search">
        <div class="box">
            <form action="">
              <label class="search" for="">
                <input class="s-i" type="search" />
                <button class="s-b" type="submit"><i class="fa fa-search"></i></button>
              </label>
            </form>
          </div>
        </li>

      </ul>
    </div>

  </div>
</div>