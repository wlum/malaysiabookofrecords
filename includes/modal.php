<!-- ================================ modal ================================== -->
<div class="reveal recordsmodal" id="recordOne" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="record-modal-image-wrap">
        <img src="images/swarovski-record.jpg">
    </div>

    <div class="record-modal-content">
        <div class="record-title">
            <h3>First Merry-Go-Round Decorated Using Swarovski Crystal</h3>
        </div>
        <hr/>
        <div class="record-desc">
            <p>The fully functional merry-go-round is 8.9metres in height and 8metres in width and weighs about 4tonnes, is
                part of the Pavilion Kuala Lumpur's Christmas decoration to celebrate the upcoming Christmas festivities.
                <br> Accepting the certificate from MBR is Dato' Joyce Yap, CEO of Pavilion Kuala Lumpur, with celebrity guest,
                Godfrey Gao, famous Taiwanese born Canadian actor and Louis Vuitton model</p>
        </div>


    </div>
</div>
<!-- End of recordsmodal -->

<!-- ================================ modal ================================== -->
<div class="reveal recordsmodal" id="recordTwo" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="record-modal-image-wrap">
        <img src="images/most-expensive-mooncake.png">
    </div>

    <div class="record-modal-content">
        <div class="record-title">
            <h3>Most Expensive Commercial Mooncake</h3>
        </div>
        <hr/>
        <div class="record-desc">
            <p>Leong Yin Pastry made it into Malaysia Book of Records for The Most Expensive Commercial Mooncake. Weighing 1.24
                kg. Measuring 17.5cm and 4cm thickness.
                <br> Dr.Leong Kok Fai use nutritious ingredients like Ginseng, Cordyceps, Bird Nests and even have 24k Gold Flakes
                inside the snowskin. Selling at RM3888.</p>
        </div>


    </div>
</div>
<!-- End of recordsmodal -->

<!-- ================================ modal ================================== -->
<div class="reveal recordsmodal" id="recordThree" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="record-modal-image-wrap">
        <img src="images/entopia.png">
    </div>

    <div class="record-modal-content">
        <div class="record-title">
            <h3>Biggest Green Wall</h3>
        </div>
        <hr/>
        <div class="record-desc">
            <p>Entopia Butterfly House set a new record for the "Biggest Green Wall", as the external facade of the new
                    Entopia building which has been covered almost completely with 88,868 planting pots with 22 species of
                    the tropical plants, measuring 2064.10 square metres and using a hydroponic watering system.
                    <br> Receiving the MBR certificate on behalf of Entopia is Mr David Goh, Chairman of Entopia, and witnessing
                    the presentation is the Yang DiPertua Negeri of Penang, T.Y.T. Tun Dato' Seri Utama (Dr.) Haji Abdul
                    Rahman Bin Haji Abbas.</p>
        </div>


    </div>
</div>
<!-- End of recordsmodal -->

<!-- ================================ what's happening modal ================================== -->
<div class="reveal recordsmodal" id="whatshappeningOne" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="record-modal-image-wrap">
        <img src="images/kl-tower-jump.jpg">
    </div>

    <div class="record-modal-content">
        <div class="record-title">
            <h3>KL Tower (Menara Kuala Lumpur)</h3>
        </div>
        <hr/>
        <div class="record-desc">
            <p>Witness another thrilling action this year. KL Tower (Menara Kuala Lumpur) once again is the host to
                      the KL Tower International Jump Malaysia (KLTIJM) 2017 held from 29th September to 2nd October 2017.
                      The excitement starts at the Sky Deck of KL Tower, located at Tower Head 5 (TH05) at the height of
                      312 metres above sea level. Watch BASE jumpers perform spectaculars jump and other daring manoeuvres.
                      This sporting event was first introduced on 3rd October 1999 in conjunction with KL Tower’s 3rd anniversary
                      celebration. Since then, it has become an annual event for KL Tower and today, 2nd October 2017, KL
                      Tower (Menara Kuala Lumpur Sdn Bhd) has marked another 3 National records in the Malaysia Book of Records
                      for 1) HIGHEST BASE JUMP PLATFORM (312 metres above sea level) 2) FIRST ROPE BASE JUMP CHALLENGE 3)
                      MOST NUMBER OF PARTICIPANTS IN A BASE JUMP CHALLENGE (105 base jumpers) Congratulations for the awesome
                      achievement</p>
        </div>


    </div>
</div>
<!-- End of event modal  -->

<!-- ================================ what's happening modal ================================== -->
<div class="reveal recordsmodal" id="whatshappeningTwo" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="record-modal-image-wrap">
        <img src="images/most-number-of-online-merchant.jpg">
    </div>

    <div class="record-modal-content">
        <div class="record-title">
            <h3>The Most Number of Online Merchant in Online Sales Event</h3>
        </div>
        <hr/>
        <div class="record-desc">
            <p>National ICT Association Malaysia or Pikom set a new record for The Most Number of Online Merchant in
                      Online Sales Event. Number registered as of Record time is 1007 merchants. Remember to go online and
                      shop in #MyCyberSale2017. Sales period from 9th to 13th Oct. Guest of honour for the event was graced
                      by Datuk Yasmin Mahmood CEO My MDEC. https://mycybersale.my/</p>
        </div>


    </div>
</div>
<!-- End of event modal  -->


<!-- ================================ what's happening modal ================================== -->
<div class="reveal recordsmodal" id="whatshappeningThree" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="record-modal-image-wrap">
        <img src="images/most-number-of-online-merchant.jpg">
    </div>

    <div class="record-modal-content">
        <div class="record-title">
            <h3>First Tiling Academy</h3>
        </div>
        <hr/>
        <div class="record-desc">
            <p>MBR is very proud to certify this sustainable CSR program by Feruni Tiling Academy as 'First Tiling Academy' in Malaysia.

Feruni Tiling Academy (FTA) holds its first ever graduation ceremony on the 4th October 2017 to commemorate the first batch of students who have passed the intensive training programme with flying colours.

FTA was established by Feruni Ceramiche as a CSR initiative with the aim to uplift the lives of the community and transform the tiling industry. This is a collaborative effort together with Construction Industry Development Board (CIDB) Holdings Sdn Bhd, Akademi Binaan Malaysia (ABM), Mapei Malaysia and Pavire (M) Sdn Bhd. It is a six-month intensive training programme focussing on tile installation methodology and best practices.

Students who have successfully completed their training will be known as “Tilistas” (professional tilers), where they will receive certification endorsed by CIDB Holdings Sdn Bhd through Akademi Binaan Malaysia.</p>
        </div>


    </div>
</div>
<!-- End of event modal  -->

<!-- ================================ event modal ================================== -->
<div class="reveal recordsmodal" id="eventOne" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="record-modal-image-wrap">
        <img src="images/akademifantasia.jpg">
    </div>

    <div class="record-modal-content">
        <div class="record-title">
            <h3>Longest Running Singing Reality Television Show</h3>
        </div>
        <hr/>
        <div class="record-desc">
            <p>he first season of Akademi Fantasia (AF) premiered on June 9, 2003 on the Astro Ria television channel.
                      12 contestants were selected and went through the studies in the academy consist of vocal, stage performance,
                      dance and motivational aspects. This year, AF once again marked another nation record as the "Longest
                      Running Singing Reality Television Show". Throughout this 14th seasons, the content of the programme
                      was not only a weekly concert taking place every weekend. It also involves AF diary, where the contestants
                      were quarantine in a secret house and all their activities were monitored by set of cameras. AF Megastar
                      assembled all AF Stars from Season 1- Season 13 in way to decide who is the best among the best!</p>
        </div>


    </div>
</div>
<!-- End of event modal  -->
