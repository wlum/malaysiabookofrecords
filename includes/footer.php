<script src="js/jquery-3.3.1.min.js">
</script>
<script src="js/vendor/foundation.min.js"></script>
<script src="js/app.js"></script>
<script src="js/slick.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="js/freewall.js"></script>
<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    var $grid = $('.grid').masonry({
        itemSelector: '.brick',
        percentPosition: true,
        columnWidth: '.grid-sizer',
        gutter: 10,
    });
    
    // Load More 
    $grid.imagesLoaded().done(function () {
        size_li = $(".category-outer .brick").length;
        var $li = $(this);
        var delay = 0;
        x = 6;
        
        $('.category-outer .brick:lt(' + x + ')').show(function () {
            setTimeout(function () {
                $li.addClass('animated fadeIn show');
                $grid.masonry().masonry();
            }, delay += 100); // delay 100 ms
        })
        
        $('.load-more').click(function () {
            event.preventDefault();
            var delay = 0;
            x = (x + 5 <= size_li) ? x + 5 : size_li;
            $('.category-outer .brick:lt(' + x + ')').show();
            setTimeout(function () {
                $li.addClass('animated fadeIn show');
                $grid.masonry().masonry();
            }, delay += 100); // delay 100 ms
        });
        
        $('.load-less').click(function () {
            $(".load-less").addClass("show");
            x = (x - 5 < 0) ? 6 : x - 5;
            $('.category-outer .brick').not(':lt(' + x + ')').hide();
            $grid.masonry().masonry();
        });
        
        if ($(".category-outer .brick").length === 0) {
            $('.load-more').fadeOut('slow');
        }
        
        // if ($('.brick:hidden').length <= 9) {
            //     $('.load-more').fadeOut('slow');
            // }
            
            
        });
        
        // Colors
        var imgColours = ['#a61a1f', '#a962a9', '#0073bd', '#f46423'];
        var elements = document.getElementsByClassName('category-record-title');
        
        var ec = 0;
        var i = 0;
        for (ec; ec < elements.length; ec++, i++) {
            elements[ec].style.backgroundColor = imgColours[i];
            
            if (i == (imgColours.length - 1)) i = -1;
            
        }
        
        // Search Button
        $(function () {
            var w = $(window).width(),
            h = $(window).height();
            
            $("#menu-main > .nav-right .search .s-b").on("click", function (e) {
                e.preventDefault();
                $("#menu-main").addClass("search-exp");
                $(".s-i").focus();
            });
            $(".s-i").blur(function () {
                // Do not hide input if contains text
                if ($(".s-i").val() === "") {
                    $("#menu-main").removeClass("search-exp");
                }
            });
            
            $("#menu-home > .nav-right .search .s-b").on("click", function (e) {
                e.preventDefault();
                $("#menu-home").addClass("search-exp");
                $(".s-i").focus();
            });
            $(".s-i").blur(function () {
                // Do not hide input if contains text
                if ($(".s-i").val() === "") {
                    $("#menu-home").removeClass("search-exp");
                }
            });
        })
        
        // BACK TO TOP
        var $backToTop = $(".back-to-top");
        $backToTop.hide();
        
        
        $(window).on('scroll', function() {
            if ($(this).scrollTop() > 100) {
                $backToTop.fadeIn();
            } else {
                $backToTop.fadeOut();
            }
        });
        
        $backToTop.on('click', function(e) {
            $("html, body").animate({scrollTop: 0}, 500);
        });
        
    });
    </script>
    
    <div class="back-to-top vertical-middle"><span><i class="fas fa-angle-up"></i>Back to<br/> Top</span></div>
    
    
    <footer>
    <div class="social-links-bg"></div>
    <div class="footer-content">
    <div class="social-links">
    <p>STAY CONNECTED</p>
    <div class="social-links-connect">
    <ul>
    <li>
    <a href="https://www.facebook.com/MalaysiaBookRecords/" target="_blank">
    <i class="fab fa-facebook"></i>
    </a>
    </li>
    <li>
    <a href="https://www.instagram.com/malaysiabookofrecords/" target="_blank">
    <i class="fab fa-instagram"></i>
    </a>
    </li>
    </ul>
    </div>
    </div>
    <p>Copyright © The Malaysia Book of Records (MBR). All Right Reserved.</p>
    </div>
    
    </footer>