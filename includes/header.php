<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="keywords" content="Malaysia, Book, Of, Records, malaysian, holder">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="MBR is set to motivate and inspire Malaysians to excel in various fields to the extent of surpassing the world's standard">
  <title><?php echo $pagetitle ; ?></title>

  <!-- Google Font  -->
  <link href="https://fonts.googleapis.com/css?family=Satisfy|Roboto+Slab:100,300,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:100i,200,300,300i,400,400i,500,600,700,800,800i,900" rel="stylesheet">

  <!-- Custom CSS -->
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">
  <link href="css/foundation.min.css" rel="preload stylesheet" as="style">
  <link href="css/slick-theme.css" rel="stylesheet">
  <link href="css/slick.css" rel="preload stylesheet" as="style">
  <link href="css/all.css" rel="stylesheet">
  <link href="css/fontawesome.css" rel="stylesheet">
  <link href="css/magnific-popup.css" rel="stylesheet">
  <link href="css/animate.css" rel="stylesheet">
  <link href="css/app.css" rel="stylesheet">
  </style>
</head>

<body>

  <!--PRELOADER START-->
  <div class="preloader">
    <div class="preloader-bounce">
      <div class="mbr-loading">
        <img src="images/malaysia-book-of-records-logo-dark.png">
      </div>
      <div class="pl-child pl-double-bounce1"></div>
      <div class="pl-child pl-double-bounce2"></div>
    </div>
  </div>
  <!--/.preloader-->
  <!--PRELOADER END-->