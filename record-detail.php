<?php 

$pagetitle = "Malaysia Books of Records"; 
require_once('includes/header.php');
require_once('includes/navigation-main.php')
?>

<div class="record-detail-outer">
    <div class="container">
        <div class="detail-category">
            <div class="detail-category-tag">
                <div class="row">
                    <nav aria-label="You are here:" role="navigation">
                        <ul class="breadcrumbs">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="record.php">Record</a>
                            </li>
                            <li><a href="human-achievements.php">Human Achievements</a></li>
                            <li class="active">
                                <span class="show-for-sr">Current: </span> Most Expensive Commercial Mooncake
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
        <div class="record-detail-feature-gallery">
            <div class="rcd-detail-banner">
                <div class="rcd-detail-banner-inner">
                    <a href="">
                        <img src="images/record-detail/most-expensive-mooncake/21122397_1951810068366829_2828886333568459098_o.jpg">
                    </a>
                </div>
                <div class="rcd-detail-banner-inner">
                    <a href="">
                        <img src="images/record-detail/most-expensive-mooncake/21122428_1951810551700114_2024780219591396820_o.jpg">
                    </a>
                </div>
            </div>
        </div>
        
        <div class="record-detail-wrap">
            <div class="row">
                <div class="large-9 columns">
                    <div class="detail-outer">
                        <div class="detail-title-wrap">
                            <div class="detail-title">
                                <h3>Most Expensive Commercial Mooncake</h3>

                                <div class="post-info">
                                        <i class="fas fa-calendar-alt"></i> 18 July 2018
                                    </div>

                                <div class="title-tag">
                                    <ul>
                                        <li><a href="#">Human Achievements</a></li>
                                    </ul>                                    
                               </div>

                                <hr/>
                            </div>
                        </div>
                        
                        <!-- Images -->
                        <div class="record-detail-image-container">
                            <div class="record-detail-image">
                                <div class="record-detail-gallery gallery">
                                    <a href="images/record-detail/most-expensive-mooncake/21122397_1951810068366829_2828886333568459098_o.jpg">
                                        <img src="images/record-detail/most-expensive-mooncake/21122397_1951810068366829_2828886333568459098_o.jpg">
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Content -->
                        <div class="record-detail-content">
                            <div class="large-12">
                                <p>Leong Yin Pastry made it into Malaysia Book of Records for The Most Expensive Commercial Mooncake.
                                    Weighing 1.24 kg. Measuring 17.5cm and 4cm thickness. Dr.Leong Kok Fai use nutritious ingredients
                                    like Ginseng, Cordyceps, Bird Nests and even have 24k Gold Flakes inside the snowskin. Selling
                                    at RM3888.</p>
                                </div>
                            </div>
                            
                            <!-- Content Gallery -->
                            <div class="record-detail-gallery">
                                <div class="record-detail-gallery--main">
                                    <a href="images/record-detail/most-expensive-mooncake/21083677_1951810571700112_1690594125377486724_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21083677_1951810571700112_1690594125377486724_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21106545_1951810015033501_6272529632857411905_n.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21106545_1951810015033501_6272529632857411905_n.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21122397_1951810068366829_2828886333568459098_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21122397_1951810068366829_2828886333568459098_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21122428_1951810551700114_2024780219591396820_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21122428_1951810551700114_2024780219591396820_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21122531_1951810955033407_2394660245140969804_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21122531_1951810955033407_2394660245140969804_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21122580_1951810628366773_1199610026736053006_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21122580_1951810628366773_1199610026736053006_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21122619_1951810701700099_863750188880566810_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21122619_1951810701700099_863750188880566810_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21122692_1951810441700125_1282242176426602315_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21122692_1951810441700125_1282242176426602315_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21125363_1951810831700086_7490247273458616776_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21125363_1951810831700086_7490247273458616776_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21125435_1951810775033425_3176981355036353051_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21125435_1951810775033425_3176981355036353051_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21125711_1951811025033400_3788938480431119800_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21125711_1951811025033400_3788938480431119800_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21167057_1951810078366828_5615741200241238449_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21167057_1951810078366828_5615741200241238449_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21167075_1951810488366787_8506641611660529828_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21167075_1951810488366787_8506641611660529828_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21167143_1951810721700097_5022738972011416066_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21167143_1951810721700097_5022738972011416066_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21167425_1951810025033500_8530666469864301721_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21167425_1951810025033500_8530666469864301721_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21167617_1951810915033411_8894234692499428590_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21167617_1951810915033411_8894234692499428590_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21167636_1951810861700083_6317945133086826658_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21167636_1951810861700083_6317945133086826658_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21167760_1951810398366796_6011014492982039738_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21167760_1951810398366796_6011014492982039738_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21167780_1951810521700117_6085340787732723626_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21167780_1951810521700117_6085340787732723626_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21199340_1951810995033403_7810758092811568607_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21199340_1951810995033403_7810758092811568607_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21199346_1951809988366837_5037019713869875143_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21199346_1951809988366837_5037019713869875143_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21246201_1951810588366777_8259347303798401722_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21246201_1951810588366777_8259347303798401722_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21246300_1951810895033413_2600511150934348438_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21246300_1951810895033413_2600511150934348438_o.jpg">
                                    </a>
                                    <a href="images/record-detail/most-expensive-mooncake/21246525_1951810378366798_6871945990604143187_o.jpg" title="Most Expensive Mooncake">
                                        <img src="images/record-detail/most-expensive-mooncake/21246525_1951810378366798_6871945990604143187_o.jpg">
                                    </a>
                                    
                                    
                                </div>
                            </div>
                            
                            
                        </div> <!-- End of detail outer -->
                        
                        
                    </div> <!-- End of row -->
                    
                    <div class="large-3 columns" >
                        <div class="record-list-wrap">
                            <h5>You might interested with other record</h5>
                            <hr/>
                        </div>
                        
                        <div class="record-listing">
                            <ol class="custom-counter">
                                <li>
                                    <a href="">
                                        <div class="record-listing-image">
                                            <img src="images/akademifantasia.jpg">
                                        </div>
                                        <div class="record-listing-info">
                                            <h5>Longest Running Singing Reality Television Show</h5>
                                            <div class="post-info"><i class="fas fa-calendar-alt"></i> 18 July 2018</div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <div class="record-listing-image">
                                            <img src="images/most-number-of-online-merchant.jpg">
                                        </div>
                                        <div class="record-listing-info">
                                            <h5>The Most Number of Online Merchant in Online Sales Event</h5>
                                            <div class="post-info"><i class="fas fa-calendar-alt"></i> 18 July 2018</div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <div class="record-listing-image">
                                            <img src="images/most-palm-print-on-wall.jpg">
                                        </div>
                                        <div class="record-listing-info">
                                            <h5>Most Number of Palm Prints Stamped on a Wall Mural</h5>
                                            <div class="post-info"><i class="fas fa-calendar-alt"></i> 18 July 2018</div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <div class="record-listing-image">
                                            <img src="images/swarovski-record.jpg">
                                        </div>
                                        <div class="record-listing-info">
                                            <h5>First Merry-Go-Round Decorated Using Swarovski Crystal</h5>
                                            <div class="post-info"><i class="fas fa-calendar-alt"></i> 18 July 2018</div>
                                        </div>
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            
            
        </div>
        
    </div>
    
    <?php require_once('includes/footer.php');?>
    <script>
        $('.rcd-detail-banner').slick({
            dots: true,
            infinite: false,
            speed: 300,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
            ]
        });
        
        $(document).ready(function () {
            $('.record-detail-gallery--main').magnificPopup({
                delegate: 'a',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                }
            });
        });
    </script>